import React, { Component } from 'react';
import axios from 'axios';
import Movie from '../components/Movie';
import _ from 'lodash';
import '../assets/App.css'

export default class Movies extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      movies: [],
      page: { page: 0, select_page: 0 },
      maxPage: 0,
    }
    this.limit = 10;
  }

  init() {
    this.setState({
      page: { page: 0, select_page: 0 },
    });
  }

  // getMovies = async () => {
  //   const {data: {data: {movies}}} = await axios.get("https://yts-proxy.now.sh/list_movies.json")
  //   console.log(movies)
  // }

  getMovies = (page) => {
    // const { page: { page, select_page } } = this.state

    console.log(page);
    this.setState({
      movies: [],
    });

    axios.get("https://yts-proxy.now.sh/list_movies.json").then((res) => {
      const paging = res.data.data.page_number
      const movies = res.data.data.movies
      console.log(movies)
      console.log(page);
      this.setState({
        movies,
        page: paging,
        isLoading: false
      })
    }).catch((e) => {
      this.setState({
        movies: []
      })
    })

  }

  pageBlockStart = () => {
    if (!this.state.page) return;
    return Math.floor(this.state.page['select_page'] / 10) * 10
  };

  pageBlockEnd = () => {
    if (!this.state.page) return;

    const max = Math.ceil((this.state.page['page'] / this.limit) * 10) + 1;
    return Math.min(this.pageBlockStart() + 10, max);
  };

  maxPage = () => {
    if (!this.state.page) return;

    return (this.state.page["page"] / this.limit) * 10;
  };

  changePage = (page) => {
    if (!this.state.page) return;
    if (this.state.page['page'] >= page) {
      if (page >= 0 && page <= this.state.maxPage) {
        axios.get("https://yts-proxy.now.sh/list_movies.json").then((res) => {
          const paging = res.data.data.page_number
          const movies = res.data.data.movies
          this.setState({
            movies,
            page: paging,
            isLoading: false,
            maxPage: res.data.data.page.page
          })
        }).catch((e) => {
          this.setState({
            movies: []
          })
        })
      }
    }
    console.log(page)
  }

  componentDidMount = () => {
    this.getMovies();
  }

  render() {
    const { isLoading, movies } = this.state;
    // const movie = this.state.movies.map((item) => (
    //   <ul key={item.id}>
    //     <li>{item.id}</li>
    //     <img src={item.medium_cover_image} alt={item.title}/>
    //   </ul>
    // ))
    return (
      <>
        <div className="container">
          {isLoading ? (
            <div className="loader">
              <span>Loading...</span>
            </div>
          ) : (
              <div className="movies">{movies.map((item) => (
                <Movie
                  key={item.id}
                  id={item.id}
                  title={item.title}
                  summary={item.summary}
                  poster={item.medium_cover_image}
                  genres={item.genres}
                />
              ))}
              </div>
            )}
        </div>
        <div className="pagination">
          {this.state.page["select_page"] !== 0 && (
            <span
              className="change"
              onClick={() =>
                this.changePage(0)
              }
            >
              처음
            </span>
          )}
          {this.state.page["select_page"] !== 0 && (
            <span
              className="change"
              onClick={() =>
                this.changePage(this.state.page.select_page + 1)
              }
            >
              이전
            </span>
          )}
          {_.range(this.pageBlockStart(), this.pageBlockEnd()).map(
            (page, idx) =>
              <span
                className="num-bar"
                key={idx}
                //   onClick={() => this.changePage(page)}
                onClick={() =>
                  this.changePage(page)
                }
                style={{
                  color:
                    page === this.state.page.select_page &&
                    "rgb(238, 75, 97)",
                }}
                data-page={page}
              >
                {page + 1}
              </span>
          )}
          {this.state.page["select_page"] < this.maxPage() && (
            <span
              className="change"
              onClick={() =>
                this.changePage(this.state.page.select_page + 1)
              }
            >
              다음
            </span>
          )}
          {this.state.page['select_page'] < this.maxPage() && (
            <span
              className="change"
              onClick={() => this.changePage(this.state.maxPage)}>끝</span>
          )}
        </div>
      </>
    )
  }
}