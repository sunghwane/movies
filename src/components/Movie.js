import React from 'react';


const Movie = ({
    id,
    year,
    title,
    summary,
    poster,
    genres
}) => {
    return (
        <div className="movie" key={id}>
            <img src={poster} alt={title} title={title} />
            <div className="movie_data">
                <h3 className="movie_title">{title}</h3>
                <h5 className="movie_year">{year}</h5>  
                {genres.map((genre, idx) => (
                    <ul className="movie_genres" key={idx}>
                        <li className="genres_genre">{genre}</li>
                    </ul>
                ))}
                <p className="movie_summary">{summary.slice(0, 180)}...</p>
            </div>
        </div>
    )
}

export default Movie